package com.devofure.uandroid;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;

import java.util.HashSet;
import java.util.List;

public class UAndroid {

    //only to know what categories there are
    public static UHardware Hardware;
    public static UFile File;
    public static UUX UX;

    /**
     * Enables strict mode. This should only be called when debugging the application and is useful
     * for finding some potential bugs or best practice violations.
     */
    @TargetApi(11)
    public static void enableStrictMode(List<Class> classes) {
        // Strict mode is only available on gingerbread or later
        if (UAndroid.hasGingerbread()) {

            // Enable all thread strict mode policies
            StrictMode.ThreadPolicy.Builder threadPolicyBuilder =
                    new StrictMode.ThreadPolicy.Builder()
                            .detectAll()
                            .penaltyLog();

            // Enable all VM strict mode policies
            StrictMode.VmPolicy.Builder vmPolicyBuilder =
                    new StrictMode.VmPolicy.Builder()
                            .detectAll()
                            .penaltyLog();

            // Honeycomb introduced some additional strict mode features
            if (UAndroid.hasHoneycomb()) {
                // Flash screen when thread policy is violated
                threadPolicyBuilder.penaltyFlashScreen();
                // For each activity class, set an instance limit of 1. Any more instances and
                // there could be a memory leak.
                for (Class classActivity : classes) {
                    vmPolicyBuilder
                            .setClassInstanceLimit(classActivity, 1);
                }
            }
            // Use builders to enable strict mode policies
            StrictMode.setThreadPolicy(threadPolicyBuilder.build());
            StrictMode.setVmPolicy(vmPolicyBuilder.build());
        }
    }

    /**
     * Go to activity
     *
     * @param context
     * @param activityClass
     */
    public static <T> void goToActivity(Class<T> activityClass, Context context) {
        context.startActivity(new Intent(context, activityClass));
    }

    /**
     * @return the max VM memory
     */
    public static int getMaxVMMemory() {
        return (int) (Runtime.getRuntime().maxMemory() / 1024);
    }

    /**
     * Uses static final constants to detect if the device's platform version is Gingerbread or
     * later.
     * November 2010: Android 2.3
     * API 9
     *
     * <p>Applications targeting this or a later release will get these
     * new changes in behavior:</p>
     * <ul>
     * <li> The application's notification icons will be shown on the new
     * dark status bar background, so must be visible in this situation.
     * </ul>
     */
    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb or
     * later.
     * February 2011: Android 3.0.
     * API 11
     *
     * <p>Applications targeting this or a later release will get these
     * new changes in behavior:</p>
     * <ul>
     * <li> The default theme for applications is now dark holographic:
     *      {@link android.R.style#Theme_Holo}.
     * <li> On large screen devices that do not have a physical menu
     * button, the soft (compatibility) menu is disabled.
     * <li> The activity lifecycle has changed slightly as per
     * {@link Activity}.
     * <li> An application will crash if it does not call through
     * to the super implementation of its
     * {@link Activity#onPause Activity.onPause()} method.
     * <li> When an application requires a permission to access one of
     * its components (activity, receiver, service, provider), this
     * permission is no longer enforced when the application wants to
     * access its own component.  This means it can require a permission
     * on a component that it does not itself hold and still access that
     * component.
     * <li> {@link Context#getSharedPreferences
     * Context.getSharedPreferences()} will not automatically reload
     * the preferences if they have changed on storage, unless
     * {@link Context#MODE_MULTI_PROCESS} is used.
     * <li> {@link android.view.ViewGroup#setMotionEventSplittingEnabled}
     * will default to true.
     * <li> {@link android.view.WindowManager.LayoutParams#FLAG_SPLIT_TOUCH}
     * is enabled by default on windows.
     * <li> {@link android.widget.PopupWindow#isSplitTouchEnabled()
     * PopupWindow.isSplitTouchEnabled()} will return true by default.
     * <li> {@link android.widget.GridView} and {@link android.widget.ListView}
     * will use {@link android.view.View#setActivated View.setActivated}
     * for selected items if they do not implement {@link android.widget.Checkable}.
     * <li> {@link android.widget.Scroller} will be constructed with
     * "flywheel" behavior enabled by default.
     * </ul>
     */
    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb MR1 or
     * later.
     * May 2011: Android 3.1.
     * API 12
     */
    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR1;
    }

    /**
     *
     * June 2011: Android 3.2.
     *
     * <p>Update to Honeycomb MR1 to support 7 inch tablets, improve
     * screen compatibility mode, etc.</p>
     *
     * <p>As of this version, applications that don't say whether they
     * support XLARGE screens will be assumed to do so only if they target
     * HONEYCOMB or later; it had been GINGERBREAD or
     * later.  Applications that don't support a screen size at least as
     * large as the current screen will provide the user with a UI to
     * switch them in to screen size compatibility mode.</p>
     *
     * <p>This version introduces new screen size resource qualifiers
     * based on the screen size in dp: see
     * {@link android.content.res.Configuration#screenWidthDp},
     * {@link android.content.res.Configuration#screenHeightDp}, and
     * {@link android.content.res.Configuration#smallestScreenWidthDp}.
     * Supplying these in &lt;supports-screens&gt; as per
     * {@link android.content.pm.ApplicationInfo#requiresSmallestWidthDp},
     * {@link android.content.pm.ApplicationInfo#compatibleWidthLimitDp}, and
     * {@link android.content.pm.ApplicationInfo#largestWidthLimitDp} is
     * preferred over the older screen size buckets and for older devices
     * the appropriate buckets will be inferred from them.</p>
     *
     * <p>Applications targeting this or a later release will get these
     * new changes in behavior:</p>
     * <ul>
     * <li><p>New {@link PackageManager#FEATURE_SCREEN_PORTRAIT}
     * and {@link PackageManager#FEATURE_SCREEN_LANDSCAPE}
     * features were introduced in this release.  Applications that target
     * previous platform versions are assumed to require both portrait and
     * landscape support in the device; when targeting Honeycomb MR1 or
     * greater the application is responsible for specifying any specific
     * orientation it requires.</p>
     * <li><p>{@link android.os.AsyncTask} will use the serial executor
     * by default when calling {@link android.os.AsyncTask#execute}.</p>
     * <li><p>{@link android.content.pm.ActivityInfo#configChanges
     * ActivityInfo.configChanges} will have the
     * {@link android.content.pm.ActivityInfo#CONFIG_SCREEN_SIZE} and
     * {@link android.content.pm.ActivityInfo#CONFIG_SMALLEST_SCREEN_SIZE}
     * bits set; these need to be cleared for older applications because
     * some developers have done absolute comparisons against this value
     * instead of correctly masking the bits they are interested in.
     * </ul>
     * @return if true, API 13
     */
    public static boolean hasHoneycombMR2() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR2;
    }

    /**
     * Uses static final constants to detect if the device's platform version is ICS or
     * later.
     *
     * October 2011: Android 4.0.
     * API 14
     * <p>Applications targeting this or a later release will get these
     * new changes in behavior:</p>
     * <ul>
     * <li> For devices without a dedicated menu key, the software compatibility
     * menu key will not be shown even on phones.  By targeting Ice Cream Sandwich
     * or later, your UI must always have its own menu UI affordance if needed,
     * on both tablets and phones.  The ActionBar will take care of this for you.
     * <li> 2d drawing hardware acceleration is now turned on by default.
     * You can use
     * {@link android.R.attr#hardwareAccelerated android:hardwareAccelerated}
     * to turn it off if needed, although this is strongly discouraged since
     * it will result in poor performance on larger screen devices.
     * <li> The default theme for applications is now the "device default" theme:
     *      {@link android.R.style#Theme_DeviceDefault}. This may be the
     *      holo dark theme or a different dark theme defined by the specific device.
     *      The {@link android.R.style#Theme_Holo} family must not be modified
     *      for a device to be considered compatible. Applications that explicitly
     *      request a theme from the Holo family will be guaranteed that these themes
     *      will not change character within the same platform version. Applications
     *      that wish to blend in with the device should use a theme from the
     *      {@link android.R.style#Theme_DeviceDefault} family.
     * <li> Managed cursors can now throw an exception if you directly close
     * the cursor yourself without stopping the management of it; previously failures
     * would be silently ignored.
     * <li> The fadingEdge attribute on views will be ignored (fading edges is no
     * longer a standard part of the UI).  A new requiresFadingEdge attribute allows
     * applications to still force fading edges on for special cases.
     * <li> {@link Context#bindService Context.bindService()}
     * will not automatically add in {@link Context#BIND_WAIVE_PRIORITY}.
     * <li> App Widgets will have standard padding automatically added around
     * them, rather than relying on the padding being baked into the widget itself.
     * <li> An exception will be thrown if you try to change the type of a
     * window after it has been added to the window manager.  Previously this
     * would result in random incorrect behavior.
     * <li> {@link android.view.animation.AnimationSet} will parse out
     * the duration, fillBefore, fillAfter, repeatMode, and startOffset
     * XML attributes that are defined.
     * <li> {@link android.app.ActionBar#setHomeButtonEnabled
     * ActionBar.setHomeButtonEnabled()} is false by default.
     * </ul>
     */
    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    /**
     * June 2010: Android 2.2
     * @return
     */
    public static boolean hasFroyo() {
        // Can use static final constants like FROYO, declared in later versions
        // of the OS since they are inlined at compile time. This is guaranteed behavior.
        return Build.VERSION.SDK_INT >= VERSION_CODES.FROYO;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT;
    }

    /**
     * Id From Raw
     *
     * @param context
     * @param fileName
     * @return
     */
    public static int getIdFromRaw(Context context, String fileName) {
        return context.getResources().getIdentifier("raw/" + fileName,
                "raw", context.getPackageName());
    }


    public static boolean getContactsEmails(List<String> contactsEmail, Context context) {
        HashSet<String> emlRecsHS = new HashSet<String>();
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[]{ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID};
        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        if (cur.moveToFirst()) {
            do {
                // names comes in hand sometimes
                //String name = cur.getString(1);
                String emlAddr = cur.getString(3);

                // keep unique only
                if (emlRecsHS.add(emlAddr.toLowerCase())) {
                    contactsEmail.add(emlAddr);
                }
            } while (cur.moveToNext());
        }
        cur.close();
        return true;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            e.printStackTrace();
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @param enable    if HomeAsUp is enable
     * @param activity, recommended to use ActionbarActivity for better backward capability
     */
    @TargetApi(VERSION_CODES.HONEYCOMB)
    public static void displayHomeAsUpEnable(boolean enable, Activity activity) {
        ((ActionBarActivity) activity).getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        /*
        if(hasHoneycomb()) {
            activity.getActionBar().setDisplayHomeAsUpEnabled(enable);
        }else if(activity instanceof ActionBarActivity){
            ((ActionBarActivity) activity).getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        }else {
            throw new IllegalArgumentException("Device Android API is lower than 11 and doesn't use " +
                    " a backward capability library like: android.support.v7.app.ActionBarActivity");
        }*/
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     */
    /*public static boolean checkGooglePlayServicesAvailable(Activity activity) {
        final int connectionStatusCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(activity);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            UAndroid.showGooglePlayServicesAvailabilityErrorDialog(activity,
                    connectionStatusCode);
            return false;
        }
        return true;
    }*/

    /**
     * Called if the device does not have Google Play Services installed.
     */
    /*private static void showGooglePlayServicesAvailabilityErrorDialog(
            final Activity activity, final int connectionStatusCode) {
        final int REQUEST_GOOGLE_PLAY_SERVICES = 0;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, activity,
                        REQUEST_GOOGLE_PLAY_SERVICES);
                dialog.show();
            }
        });
    }*/

    /**
     * Gets the size of the ActionBar, work when the ActionBars id is android.R.attr.actionBarSize
     *
     * @param context
     * @return size of the ActionBar else 0 if there could not get any size
     */
    public int getActionBar(Context context) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)) {
            return TypedValue.complexToDimensionPixelSize(
                    typedValue.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }
}
