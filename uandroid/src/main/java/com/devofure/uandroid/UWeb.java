package com.devofure.uandroid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UWeb {
    public static JSONObject toJson(String json) throws JSONException {
        return new JSONObject(json);
    }

    public static JSONArray toJsonArray(String json) throws JSONException {
        return new JSONArray(json);
    }

    public static String[] toStringArray(JSONArray jsonArray) throws JSONException {
        String[] strings = new String[jsonArray.length()];
        for (int index = 0; index < jsonArray.length(); index++) {
            strings[0] = jsonArray.get(index).toString();
        }
        return strings;
    }
}
