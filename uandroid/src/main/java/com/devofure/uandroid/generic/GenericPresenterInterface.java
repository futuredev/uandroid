package com.devofure.uandroid.generic;

/**
 *
 */
public interface GenericPresenterInterface {
    void init(GenericFragment viewFragment);
}
