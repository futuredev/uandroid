package com.devofure.uandroid.generic;

import android.os.Bundle;

/**
 * A presenter that really is a Fragment, uses to have data even when the app is rotated,
 * if one mode is used the a common java object could do the trick to be the presenter
 */
public abstract class GenericPresenter
        extends GenericFragment
        implements GenericPresenterInterface {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);
    }

}
