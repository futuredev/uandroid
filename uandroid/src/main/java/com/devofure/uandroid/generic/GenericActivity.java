package com.devofure.uandroid.generic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;

/**
 * a generic Activity with ActonBarActivity
 */
public abstract class GenericActivity
        extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * gets Fragment with tha tag name
     *
     * @param tagName : a tag to identify the fragment from the fragmentManager
     * @return : if it found then it returns the fragment else null
     */
    public Fragment getFragment(String tagName) {
        return getSupportFragmentManager().findFragmentByTag(tagName);
    }

}
