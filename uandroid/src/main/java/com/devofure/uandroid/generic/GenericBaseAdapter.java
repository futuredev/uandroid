package com.devofure.uandroid.generic;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.devofure.uandroid.R;

import java.util.List;

/**
 * a Generic Base adapter with the most best practice in list adapter
 */
public abstract class GenericBaseAdapter extends BaseAdapter {

    private List mList;

    public GenericBaseAdapter(List list) {
        if (list == null) {
            throw new NullPointerException("list is null");
        }
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        if (row == null) {
            row = inflateItemView();
            row.setTag(newInstanceViewHolder(position, row, parent));
        }
        recycleViewHolder(position, row, (ViewHolder) row.getTag(), parent);
        row.setId(position);
        row.setTag(R.id.TAG_ITEM_ID, getItemIDTag(position));
        loadImages(position, (ViewHolder) row.getTag(), parent);
        return row;
    }

    /**
     * to be implemented, gets the Item Id to be identify tha a real Id from a database.
     *
     * @param position the position/index in the list
     * @return it could be any object
     */
    protected abstract Object getItemIDTag(int position);

    /**
     * inflates only the Item View
     *
     * @return the inflated view
     */
    protected abstract View inflateItemView();

    /**
     * instance a new View holder for the asked position
     *
     * @param position
     * @param row
     * @param parent
     * @return A view holder that contains the views of one item
     */
    protected abstract ViewHolder newInstanceViewHolder(int position, View row, ViewGroup parent);

    /**
     * recycles the view holder
     *
     * @param position   of the item
     * @param row viewHolder that holds every view in the item
     * @param viewHolder that will be recycle
     * @param parent     parent of the view
     */
    protected abstract void recycleViewHolder(int position,View row, ViewHolder viewHolder, ViewGroup parent);

    /**
     * loads all the images from an item view if there are any.
     * It is recommended to use the ImageManager
     *
     * @param position
     * @param tag
     * @param parent
     */
    protected abstract void loadImages(int position, ViewHolder tag, ViewGroup parent);

    /**
     * ViewHolder that uses to be inherited
     */
    protected abstract class ViewHolder {
        public ViewHolder(View viewItem) {
            bindItemView(viewItem);
        }

        /**
         * binds only the view with their resource ids
         *
         * @param itemView
         */
        protected abstract void bindItemView(View itemView);
    }

}