package com.devofure.uandroid.generic;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Generic Fragment to develop with MVP and other good practice architecture
 */
public abstract class GenericFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Here is recommended to initiate the presenter
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /**
     * a good place to load data, the app should start fast as possible and then load data async
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * should be good to remove any listener
     */
    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * all async fragment should have an update method to update important data
     */
    public interface AsyncView {
        public void update();
    }

    /**
     * Loading view to show when data is being loaded or computed
     */
    public interface LoadingView {
        void startLoadingView();

        void stopLoadingView();
    }

    /**
     * message shown when no data is available
     */
    public interface EmptyViewMessenger {
        void disableEmptyViewMessage();

        void enableEmptyViewMessage();

        void setEmptyTextMessage(String message);
    }


}
