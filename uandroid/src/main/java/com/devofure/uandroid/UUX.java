package com.devofure.uandroid;

import android.content.Context;
import android.widget.Toast;

public class UUX {

    /**
     * show a long Toast
     *
     * @param context
     * @param message , string message
     * @deprecated use {@link #showLongToast(Context, int)}
     */
    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * show a short Toast
     *
     * @param context
     * @param message
     * @deprecated use {@link #showShortToast(Context, int)}
     */
    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * show a Long Toast
     *
     * @param context
     * @param stringId , id resource of String
     */
    public static void showLongToast(Context context, int stringId) {
        Toast.makeText(context, context.getString(stringId), Toast.LENGTH_LONG)
                .show();
    }

    /**
     * show a Short Toast
     *
     * @param context
     * @param stringId , id resource of String
     */
    public static void showShortToast(Context context, int stringId) {
        Toast.makeText(context, context.getString(stringId), Toast.LENGTH_SHORT)
                .show();
    }
}
