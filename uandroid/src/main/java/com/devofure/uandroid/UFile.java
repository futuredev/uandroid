package com.devofure.uandroid;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.os.StatFs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;

public class UFile {

    /**
     * File will be store in private mode
     *
     * @param context
     * @param data
     * @param fileName
     * @throws IOException
     */
    public static void storeStringToFile(Context context, String data, String fileName) throws IOException {
        FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_PRIVATE);
        fOut.write(data.getBytes());
        fOut.close();
    }


    /**
     * @param context
     * @param fileName
     * @return
     */
    public static File getPathFromFileName(Context context, String fileName) {
        return new File(context.getFilesDir(), fileName);
    }

    /**
     * @param context
     * @param idResource
     * @return
     */
    public static File getPathFromFileName(Context context, int idResource) {
        return new File(context.getFilesDir(), context.getString(idResource));
    }

    /**
     * @param context
     * @param fieName
     * @return
     */
    public static boolean isFileExisting(Context context, String fieName) {
        return context.getFileStreamPath(fieName).exists();
    }

    /**
     * @param context
     * @param idResource
     * @return
     */
    public static boolean exists(Context context, int idResource) {
        return context.getFileStreamPath(context.getString(idResource)).exists();
    }


    /**
     * Get the external app cache directory.
     *
     * @param context The context to use
     * @return The external cache dir
     */
    @TargetApi(VERSION_CODES.FROYO)
    public static File getExternalCacheDir(Context context) {
        if (UAndroid.hasFroyo()) {
            return context.getExternalCacheDir();
        }
        // Before Froyo we need to construct the external cache dir ourselves
        final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    /**
     * @param fileName
     * @param object
     * @param context
     * @throws IOException
     */
    public static void writeSerializedObject(String fileName, Object object, Context context)
            throws IOException {
        ObjectOutputStream objectOutputStream = null;
        FileOutputStream outStream = null;
        try {
            outStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            objectOutputStream = new ObjectOutputStream(outStream);
            objectOutputStream.writeObject(object);
        } finally {
            if (objectOutputStream != null)
                objectOutputStream.close();
            if (outStream != null)
                outStream.close();
        }
    }

    /**
     * @param fileName
     * @param context
     * @return object
     * @throws OptionalDataException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Object getSerializedObject(String fileName, Context context)
            throws OptionalDataException, ClassNotFoundException, IOException {
        Object object = null;
        ObjectInputStream objectInput = null;
        FileInputStream inputStream = null;
        try {
            inputStream = context.openFileInput(fileName);
            objectInput = new ObjectInputStream(inputStream);
            object = objectInput.readObject();

        } finally {
            if (objectInput != null)
                objectInput.close();
            if (inputStream != null)
                inputStream.close();
        }
        return object;
    }

    /**
     * Check how much usable space is available at a given path.
     *
     * @param path The path to check
     * @return The space available in bytes
     */
    public static long getUsableSpace(File path) {
        if (UAndroid.hasGingerbread()) {
            return path.getUsableSpace();
        }
        final StatFs stats = new StatFs(path.getPath());
        return (long) stats.getBlockSize() * (long) stats.getAvailableBlocks();
    }
}
