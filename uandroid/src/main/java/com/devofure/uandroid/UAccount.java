package com.devofure.uandroid;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;


public class UAccount {

    public static final int REQUEST_CODE_AUTH = 8844;
    public static final int REQUEST_CODE_RECOVER_ACCOUNT = 5187;
    private static final String KEY_TOKEN = "KEY_TOKEN";
    private static final String KEY_EMAIL = "KEY_EMAIL";
    private static final String KEY_REG_ID = "KEY_REG_ID";
    private static final String PROPERTY_APP_VERSION = "PROPERTY_APP_VERSION";
    private static final String KEY_DISPLAY_NAME = "KEY_DISPLAY_NAME";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String KEY_LOCAL_TOKEN = "KEY_LOCAL_TOKEN";
    private static final String KEY_LOCAL_SIGN_IN = "KEY_LOCAL_SIGN_IN";

    private static String email = "";
    private static String token = "";
    private static String regId = "";
    private static String displayName = "";
    private static String localUserToken;
    private static boolean isUserLocalSignin = false;

    /**
     * retrieves the users email from the preferences
     *
     * @param context
     * @return the email that was saved from either storeEmail or storeAccountData(),
     */
    public static String getEmail(Context context) {
        if (email != null && !email.isEmpty()) {
            return email;
        }
        // Get from shared preferences
        final SharedPreferences preferences = getPreferences(context.getApplicationContext());
        email = preferences.getString(KEY_EMAIL, "");
        return email;
    }

    /**
     * store the user email
     * @param email
     * @param context
     */
    public static void storeEmail(String email, Context context) {
        UAccount.email = email;
        // save email in preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.putString(KEY_EMAIL, email);
        editor.apply();
    }

    /**
     * get the token saved from storeToken or storeAccountData()
     * @param context
     * @return
     */
    public static String getToken(Context context) {
        if (token != null && !token.isEmpty()) {
            return token;
        }
        // Get from shared preferences
        final SharedPreferences preferences = getPreferences(context.getApplicationContext());
        UAccount.token = preferences.getString(KEY_TOKEN, "");
        return token;
    }

    /**
     * stores a token, it is NOT the same token as local token, see {@link #signinLocal(String, Context) signinLocal}
     * @param token a token to be store
     * @param context
     */
    public static void storeToken(String token, Context context) {
        if (token != null && token.length() < 500) {
            throw new IllegalArgumentException("token is or long : " + token.length());
        } else if (token == null) {
            token = "";
        }
        // save token in preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.putString(KEY_TOKEN, token);
        editor.apply();
        UAccount.token = token;
    }

    /**
     * the userToken is the email of the user that have signed in, will later authentificated with the email stored in the UAccount User Email
     *
     * @param userToken the email of the user that  is already authenticated with the server
     */
    public static void signinLocal(String userToken, Context context) {
        // save email in preferences
        final SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(KEY_LOCAL_TOKEN, userToken);
        editor.putBoolean(KEY_LOCAL_SIGN_IN, true);
        editor.apply();
        UAccount.localUserToken = userToken;
        UAccount.isUserLocalSignin = true;
    }

    public static boolean isSigninLocal(Context context) {
        if ((localUserToken != null) && localUserToken.equals(email)
                && isUserLocalSignin) {
            return true;
        }
        final SharedPreferences preferences = getPreferences(context.getApplicationContext());
        localUserToken = preferences.getString(KEY_LOCAL_TOKEN, "");
        isUserLocalSignin = preferences.getBoolean(KEY_LOCAL_SIGN_IN, false);
        return (localUserToken != null && !localUserToken.isEmpty() && isUserLocalSignin);
    }

    /**
     * removes the local token,
     * the common token if there is one stored,
     * the user email, dsplay name,
     * registration id and the signIn Auth.
     *
     * @param context
     */
    public static void signOutLocal(Context context) {
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.remove(KEY_LOCAL_TOKEN);
        editor.remove(KEY_LOCAL_SIGN_IN);
        editor.remove(KEY_DISPLAY_NAME);
        editor.remove(KEY_REG_ID);
        editor.remove(KEY_EMAIL);
        editor.remove(KEY_TOKEN);
        editor.apply();
        UAccount.localUserToken = null;
        UAccount.isUserLocalSignin = false;
        UAccount.email = null;
        UAccount.displayName = null;
        UAccount.regId = null;
        UAccount.token = null;
    }

    /**
     *
     * @param token
     * @param email
     * @param context
     */
    public static void storeAccountData(String token, String email, Context context) {
        // save token in preferences
        UAccount.email = email;
        UAccount.token = token;
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_EMAIL, email);
        editor.apply();
    }

    /**
     *
     * @param displayName
     * @param context
     */
    public static void storeDisplayName(String displayName, Context context) {
        UAccount.displayName = displayName;
        // save display in preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.putString(KEY_DISPLAY_NAME, displayName);
        editor.apply();
    }

    /**
     *
     * @param context
     */
    public static void removeToken(Context context) {
        // Remove token from preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.remove(KEY_TOKEN);
        editor.apply();
        token = null;
    }

    /**
     *
     * @param context
     */
    public static void removeEmail(Context context) {
        // Remove email from preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.remove(KEY_EMAIL);
        editor.apply();
        email = null;
    }

    /**
     *
     * @param context
     */
    private static void removeRegId(Context context) {
        // Remove email from preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.remove(KEY_REG_ID);
        editor.apply();
        regId = null;
    }

    /**
     * @param context
     * @return true if a registration di is stored in the app
     */
    public static boolean isAccountGCMRegistered(Context context) {
        regId = getGCMRegistrationId(context);
        return (regId != null && !regId.isEmpty());
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    public static void storeGCMRegistrationId(Context context, String regId) {
        if (regId.length() > 500) {
            throw new IllegalArgumentException("regId is too long");
        }
        int appVersion = UAndroid.getAppVersion(context.getApplicationContext());
        SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.putString(KEY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
        UAccount.regId = regId;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    public static String getGCMRegistrationId(Context context) {
        if (regId != null && regId.length() > 1) {
            return regId;
        }
        final SharedPreferences preferences = getPreferences(context.getApplicationContext());
        regId = preferences.getString(KEY_REG_ID, "");
        if (regId.isEmpty()) {
            return regId;
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        if (preferences.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE)
                != UAndroid.getAppVersion(context.getApplicationContext())) {
            return null;
        }
        return regId;
    }


    /**
     * deletes all user Authentication from the app
     *
     * @param context
     */
    private static void deleteUserAuthentication(Context context) {
        removeEmail(context);
        removeToken(context);
        removeRegId(context);
        removeDisplayName(context);
    }

    private static void removeDisplayName(Context context) {
        final SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.remove(KEY_DISPLAY_NAME);
        editor.apply();
        displayName = null;
    }


    private static String getPath(Context context) {
        //return key.isEmpty() ?  context.getPackageName() : context.getPackageName() + "." + key;
        return context.getApplicationContext().getPackageName();
        //return key.isEmpty() ?  "preference" : "preference" + "."+ key;
        //return "preferences";
    }


    /**
     * Count Google accounts on the device.
     * GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE
     */
    public static int countGoogleAccounts(Context context, String accountType) {
        AccountManager am = AccountManager.get(context.getApplicationContext());
        Account[] accounts = am
                .getAccountsByType(accountType);
        if (accounts == null || accounts.length < 1) {
            return 0;
        } else {
            return accounts.length;
        }
    }

    public static String getPhoneNumber(Context context) {
        return ((TelephonyManager) context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private static SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(getPath(context.getApplicationContext()),
                Context.MODE_PRIVATE);
    }

    /**
     * runs on a separated thread
     *
     * @param activity                      where the error will bounce back after the user retrieves the forgotten password
     * @param onRetrieveTokenResultListener the listener that interacts to the activity
     */
    /*public static void retrieveGooglePlusToken(Activity activity, OnRetrieveTokenResultListener onRetrieveTokenResultListener) throws NoEmailException {
        retrieveGooglePlusToken(activity, onRetrieveTokenResultListener, getEmail(activity));
    }*/

    /**
     * @param activity                      where the error will bounce back after the user retrieves the forgotten password
     * @param email                         email that will retrieve the token
     * @param onRetrieveTokenResultListener the result will call the listener
     */
    /*public static void retrieveGooglePlusToken(
            final Activity activity,
            final OnRetrieveTokenResultListener onRetrieveTokenResultListener,
            final String email) throws NoEmailException {
        if (email == null || email.isEmpty()) {
            throw new NoEmailException("No Email To Authenticate");
        }

        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                boolean success = false;
                try {
                    Log.e("email user ", email);
                    token = GoogleAuthUtil.getToken(
                            activity,
                            email,
                            "oauth2:" + Scopes.PLUS_LOGIN + " https://www.googleapis.com/auth/plus.profile.emails.read"
                    );
                    if (token != null) {
                        storeAccountData(token, email, activity);
                        success = true;
                    }
                } catch (GooglePlayServicesAvailabilityException playEx) {
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                            playEx.getConnectionStatusCode(),
                            activity,
                            REQUEST_CODE_AUTH);
                    // Use the dialog to present to the user.
                } catch (UserRecoverableAuthException recoverableException) {
                    Intent recoveryIntent = recoverableException.getIntent();
                    // Use the intent in a custom dialog or just startActivityForResult.
                    activity.startActivityForResult(recoveryIntent, REQUEST_CODE_RECOVER_ACCOUNT);
                } catch (GoogleAuthException authEx) {
                    // This is likely unrecoverable.
                    authEx.printStackTrace();
                } catch (IOException ioEx) {
                    ioEx.printStackTrace();
                }
                return success;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);
                if (success) {
                    onRetrieveTokenResultListener.onRetrieveTokenSuccess();
                } else {
                    onRetrieveTokenResultListener.onRetrieveTokenFailed();
                }
            }
        }.execute();
    }*/

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     * <p/>
     * Verifies OAuth2 token access for the application and Google account
     * combination with the {@code AccountUtils} and the Play Services installed
     * application. If the appropriate OAuth2 access hasn't been granted (to
     * this application) then the task may fire an {@code Intent} to request
     * that the user approve such access. If the appropriate access does exist
     * then the button that will let the user proceed to the next activity is
     * enabled.
     *
     * @param context
     * @param senderId the project id of the appengine
     */
    /*public static void registerGCMDevice(final Context context, final String senderId, final OnRegisterGCMDeviceResult onRegisterGCMDeviceResult) {
        new android.os.AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    if (regId == null || regId.isEmpty()) {
                        if (gcm == null) {
                            gcm = GoogleCloudMessaging.getInstance(context);
                        }
                        regId = gcm.register(senderId);
                        if (regId == null || regId.isEmpty()) {
                            throw new Exception("registration id is " + regId);
                        }
                        // Persist the regID - no need to register again.
                        UAccount.storeGCMRegistrationId(context, regId);
                    }
                } catch (IOException ex) {
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                    ex.printStackTrace();
                    return false;
                } catch (GoogleAuthException e) {
                    e.printStackTrace();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (success) {
                    onRegisterGCMDeviceResult.OnRegisterGCMDeviceSuccess();
                } else {
                    onRegisterGCMDeviceResult.OnRegisterGCMDeviceFailed();
                }
            }
        }.execute(null, null, null);
    }*/

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    /*public boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(context.getApplicationContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context.getApplicationContext(),
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //Log.i("checkPlayServices", "This device is not supported.");
            }
            return false;
        }
        return true;
    }*/

    public static void storeStringData(String key, String value, Context context) {
        // save display in preferences
        final SharedPreferences.Editor editor = getPreferences(context.getApplicationContext()).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String retrieveStringData(String key, String defaultValue, Context context) {
        final SharedPreferences preferences = getPreferences(context.getApplicationContext());
        if(preferences.contains(key)){
            return preferences.getString(key, defaultValue);
        }
        return defaultValue;
    }

    public interface OnRetrieveTokenResultListener {
        void onRetrieveTokenSuccess();

        void onRetrieveTokenFailed();
    }

    public interface OnSignInGoogleResultListener {
        void onSignInResultSuccess();

        void onSignInGoogleFailed();

        void onSignOutGoogle();
    }

    public interface OnRegisterGCMDeviceResult {

        void OnRegisterGCMDeviceSuccess();

        void OnRegisterGCMDeviceFailed();
    }

    private static class NoEmailException
            extends Exception {
        public NoEmailException() {
            super();
        }

        public NoEmailException(String message) {
            super(message);
        }

        public NoEmailException(String message, Throwable cause) {
            super(message, cause);
        }

        public NoEmailException(Throwable cause) {
            super(cause);
        }
    }
}
