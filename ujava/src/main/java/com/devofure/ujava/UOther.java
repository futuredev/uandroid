package com.devofure.ujava;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

public class UOther {

    public static double getHighest(double[] numbers){
        double highest = 0;
        for(double number: numbers){
            if(highest < number){
                highest = number;
            } 
        }
        return highest;
    }

    public static Double getHighestInDoubleList(List<Double> numbers){
        Double highest = (double) 0;
        for(Double number: numbers){
            if(highest < number){
                highest = number;
            }
        }
        return highest;
    }

    public static int getHighest(int[] numbers){
        int highest = 0;
        for(int number: numbers){
            if(highest < number){
                highest = number;
            }
        }
        return highest;
    }
    public static long getHighest(long[] numbers){
        long highest = 0;
        for(long number: numbers){
            if(highest < number){
                highest = number;
            }
        }
        return highest;
    }

    public static String getHighest(String[] numbers){
        String highest = "0";
        for(String number: numbers){
            if(highest.compareTo(number) < 0){
                highest = number;
            }
        }
        return highest;
    }

    public static String getHighestInStringList(List<String> numbers){
        String highest = "0";
        for(String number: numbers){
            if(highest.compareTo(number) < 0){
                highest = number;
            }
        }
        return highest;
    }

    /**
     *
     * @param input
     * @param deleteMe
     * @return
     */
    private static Object[] removeElements(Object[] input, String deleteMe) {
        List result = new LinkedList();
        for(Object item : input) {
            if (!deleteMe.equals(item))
                result.add(item);
        }
        return result.toArray(input);
    }

    /**
     *
     * @param original
     * @param index
     * @return
     */
    private static Object[] removeElement(Object[] original, int index){
        Object[] n = new Object[original.length - 1];
        System.arraycopy(original, 0, n, 0, index );
        System.arraycopy(original, ++index, n, index, original.length - --index);
        return n;
    }

    /**
     * A hashing method that changes a string (like a URL) into a hash suitable for using as a
     * disk filename.
     */
    public static String hashKey(String key) {
        String cacheKey;
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(key.getBytes());
            cacheKey = bytesToHexString(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    /**
     * @param bytes
     * @return
     */
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
}
