package com.devofure.ujava;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class UTime {

    // MILLIS
    public static long daysToMillis(long days) {
        return TimeUnit.DAYS.toMillis(days);
    }

    public static long hoursToMillis(long hours) {
        return TimeUnit.HOURS.toMillis(hours);
    }

    public static long minutesToMillis(long minutes) {
        return TimeUnit.MINUTES.toMillis(minutes);
    }

    public static long secondsToMillis(long seconds) {
        return TimeUnit.SECONDS.toMillis(seconds);
    }

    public static long toMillis(long days, long hours, long minutes, long seconds) {
        return daysToMillis(days) + hoursToMillis(hours) + minutesToMillis(minutes) + secondsToMillis(seconds);
    }

    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public static long currentTimeMillisPlus(long days, long hours, long minutes, long seconds) {
        return System.currentTimeMillis() + toMillis(days, hours, minutes, seconds);
    }

    public static long millisPlus(long millis, long days, long hours, long minutes, long seconds) {
        return millis + toMillis(days, hours, minutes, seconds);
    }

    public static String millisToFormat(long millis, String format) {
        return new SimpleDateFormat(format).format(millis);
    }

    public static String millisToDateString(long millis) {
        return (new SimpleDateFormat("yyyy-MM-dd").format(millis));
    }

    public static String millisToDateTimeString(long millis) {
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm").format(millis));
    }

    public static String currentTimeToString() {
        return (new SimpleDateFormat("HH:mm:ss:SSS").format(UTime.currentTimeMillis()));
    }

    public static Calendar millisToCalendar(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar;
    }

    public Calendar firstDayOfMonth(int year, int month){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar;
    }


    public Calendar lastDayOfMonth(int year, int month){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar;
    }

    public static Calendar currentDate() {
        return Calendar.getInstance();
    }
}
