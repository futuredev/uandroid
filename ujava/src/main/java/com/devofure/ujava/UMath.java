package com.devofure.ujava;

public class UMath {
    /**
     * @param amount
     * @param total
     * @return
     */
    public static double getPercent(double amount, double total) {
        if (amount == 0 || total == 0) {
            throw new ArithmeticException("Illegal value: 0 ");
        }
        return (amount / total) * 100;
    }
}
